package com.test1;

import org.apache.log4j.*;

public class Log4jDemo {

	private static Logger log = LogManager.getLogger(Log4jDemo.class.getName());
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		log.debug("i am from debugging");
		log.info("i am from info");
		log.error("i am from error");
		
		log.fatal("i am from fatal");
		log.warn("i am from warning");

	}

}
