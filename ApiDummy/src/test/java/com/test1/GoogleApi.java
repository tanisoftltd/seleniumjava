package com.test1;

import org.testng.annotations.*;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import static org.hamcrest.Matchers.*;

import static io.restassured.RestAssured.*;



public class GoogleApi {
	
		
	@Test
	public void getrestaurant() {
		
		//baseURL
		RestAssured.baseURI = "https://maps.googleapis.com";
		
		given().
			param("query","restaurants+in+Sydney").
			//param("radius","500").
			//param("type","restaurant").
			param("key","AIzaSyBdHW3KYTJl_dVsGmegzk7BgAMRHBllv34").
		when().
			get("/maps/api/place/textsearch/xml").
		then().
			assertThat().
			statusCode(200).and().
			contentType(ContentType.XML).and().
			body("PlaceSearchResponse\result[0]\\name",equalTo("Tetsuya's Restaurant"));

		//.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=AIzaSyBdHW3KYTJl_dVsGmegzk7BgAMRHBllv34")
		//https://maps.googleapis.com/maps/api/placejava.lang.AssertionError: 1 expectation failed.
		
		
	}

}
