package com.test1;
import org.testng.Assert;
import org.testng.annotations.*;
import groovyjarjarantlr.collections.List;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import java.util.*;


import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.codehaus.groovy.control.ResolveVisitor.*;
import static io.restassured.response.Response.*;
import io.restassured.matcher.*;


/* REST METHODS : CONNECT - uses https://, 
 * OPITONS, TRACE, DELETE, PUT, POST, HEAD 
 * GET - uses http://
 * 
 * 
 */

//course by The zero By Debarghya in youtube. 
//sheetal singh --https://www.youtube.com/watch?v=AbJrfP4ziIk&t=70s
/*
 * 
rest assured:

1). Create Msvaen project,
2). Delete artifacts from  src/main and src/test java folders.
3). Open POM.xml
4). add TESTNG repository
5). rest assured. so.rest-assured
6). add another dependence called Json Path, use IO.rest.assured jason path.
7). Json schema validator maven repository. 
8). xml path maven dependency.
9). Hamcrest java maven dependence

Step10: Make a maven build to load all the dependencies. 
*/


public class TestOne {

	//@Test
	public void test1() {
	
			given().get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22")
			.then().statusCode(200);
			given().log().all();
			
	}
	
	//@Test
	public void test2(){
		
		when().get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22")
		.then().statusCode(200);
	}
	//@Test
	public void test3() {
		
		Response response = when().get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22");
		Assert.assertEquals("200", response.statusCode());
	}
	
	//@Test
	public void test4() {
		
		given().get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22")
		.then().statusCode(200).log().all();
	}
	
	//@Test(description = "Logging only the body of response")
	public void test5() {
		
		given().get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22")
		.then().statusCode(200).log().body();
	}
	
	//@Test(description = "Writing test with Parameter and Validating particular item in JSON body..")
	public void test6() {
		
		given().param("q", "London,uk").param("appid","b1b15e88fa797225412429c1c50c122a1")
		.get("http://samples.openweathermap.org/data/2.5/weather?").then().assertThat()
		.body("name", equalTo("London"));
	}
	
	
	//@Test(description = "Using Header for authorisations ..")
	public void test7() {
		
		given().param("q", "London,uk").param("appid","b1b15e88fa797225412429c1c50c122a1")
		.header("Someval","somedata")
		.get("http://samples.openweathermap.org/data/2.5/weather").then().assertThat()
		.body("name", equalTo("London"));
	}
	
	//@Test(description = "Validate multiple nodes in body ..")
	public void test8() {
		
		given().param("q", "London,uk").param("appid","b1b15e88fa797225412429c1c50c122a1")
		.header("Someval","somedata")
		.get("http://samples.openweathermap.org/data/2.5/weather").then().assertThat()
		.body("name", equalTo("London")).body("Base", equalTo("stations")).log().all();
	}
	
	
	//@Test(description = "xpath approach contain string")
	public void test9() {
		
		given().param("q", "London,uk").param("appid","b1b15e88fa797225412429c1c50c122a1")
		.get("http://samples.openweathermap.org/data/2.5/weather").then()
		.body("name", containsString("London"));
	}
	
	//@Test(description = "equaltoIgnoreCase")
	public void test10() {
		
		given().param("q", "London,uk").param("appid","b1b15e88fa797225412429c1c50c122a1")
		.get("http://samples.openweathermap.org/data/2.5/weather").then()
		.body("name", equalToIgnoringCase("London"));
	}
	
	//@Test(description = "xml reponse-xpath")
	public void test11() {
		
		given().param("q", "London,uk").param("appid","b1b15e88fa797225412429c1c50c122a1")
		.get("http://samples.openweathermap.org/data/2.5/weather").then()
		.body(hasXPath("/current/city/country"), containsString("GB"));
	}
	
	//@Test (description = "set up root element and use for rest")
	public void testWithRoot() {
		
		get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22")
		.then().
			root("sys")
			.body("country",  is("US"))
			.body("id", is("392"));
	}
	//@Test (description = "set up root element and detach root element")
	public void testDetachRoot() {
		
		get("http://samples.openweathermap.org/data/2.5/weather?zip=94040,us&appid=b6907d289e10d714a6e88b30761fae22")
		.then().
			root("sys")
			.body("country",  is("US"))
			.detachRoot("sys")
			.body("name", is("Mountain View"));
	}
	
	//@Test
	public void testExtractDetailsUsingPath() {
		
		String href=
				when().get("http://jsonplaceholder.typicode.com/photos/1")
			.then()
				.contentType(ContentType.JSON)
				.body("albumid", equalTo(1))
			.extract()
				.path("url");
		System.out.println(href);
		
		when().get(href).then().statusCode(200);

	}
	
	// this will verify the response schema with predefined or existing schema in file
	// json file path need to be in resource folder path. 
	
	//@Test
	public void testSchema() {
		
		given().get("http://jsonplaceholder.typicode.com/photos/1")
			.then()
				.assertThat().body(matchesJsonSchemaInClasspath("test3_geo_schema123.json"));

	}
	
	//@Test(description = "extract string and getting further details using Json path")
	public void testJsonpath() {
	
		String responseAsString = when().get("http://jsonplaceholder.typicode.com/photos")
				.then()
				.extract().asString();
		System.out.println(responseAsString);
		//Groovy scripting
		//List<Integer> albumid = from(responseAsString).get("id");
		
	}
	
	//COOKIES ADN HEADER PATH.
	//@Test(description ="To get response headers")
	public void testResponseHeaders() {
		
		Response response = get("http://jsonplaceholder.typicode.com/photos");
		
		//to get single header
		String HeaderCFRay = response.getHeader("CF-RAY");
		
		//to get all header
		Headers headers = response.getHeaders();
		//for (Headers h:headers) {
			//System.out.println(h.getName()+ ":" + h.getValues());
		//}
		
	}
	

	
	//Get cookies
	//@Test
	public void testCookies() {
		Response response = get("http://jsonplaceholder.typicode.com/photos");
		
		Map<String, String> cookies = response.getCookies();
		
		for (Map.Entry<String, String> entry : cookies.entrySet()) {
			System.out.println(entry.getKey()+":" + entry.getValue());
		}
	}
	
	/*
	 * public void myMethod(String... strings){
    for(String whatever : strings){
        // do what ever you want
    	}

    // the code above is is equivalent to
    for( int i = 0; i < strings.length; i++){
        // classical for. In this case you use strings[i]
    	}
	}
	 * 
	 */
	
	//@Test //see above code
	public void UseHashMapString_newloop_Syntax() {
			
		HashMap<String, String> coocked = new HashMap<String, String>();
		
		coocked.put("1", "One");
		coocked.put("2", "Two");
		coocked.put("3", "Three");
		
		for (HashMap.Entry<String, String> entry : coocked.entrySet()) {
			
			System.out.println(entry.getKey()+":" + entry.getValue());
		}
		
		int[] test = new int[3];
		test[0] = 10;
		test[1]=20;
		test[2]=30;
		
		for (int subbu : test) {
			System.out.println(subbu);
		}
		
	    // the code above is is equivalent to
	    //for( int i = 0; i < strings.length; i++){
	        // classical for. In this case you use strings[i]
	    	//}
		//}
	}
	

	//@Test(description = "CONNECT used for https reuqest")
	public void testConnectRequest() {
		
		when()
			.request("CONNECT","https://api.fornts.com/rest/json/Accounts/")
		.then()
			.statusCode(400);
	}
	
	
	//Query Parameters
	// These will be used with GET request. 
	
	//@Test
	public void testQueryParameter() {
		
		
		given()
			.queryParam("A","A value")
			.queryParam("B",  "B values")
		.when()
			.post("http://api.fonts.com/rest/json/Domains/")
		.then()
			.statusCode(400);
	}
	//Form Parameters
	//These will be used for POST request.
	
	//@Test
	public void testFormParameter() {
		
		
		given()
			.formParam("A","A value")
			.formParam("B",  "B values")
		.when()
			.post("http://api.fonts.com/rest/json/Domains/")
		.then()
			.statusCode(400);
	}
	
	//NOTE: Param passing using - can be used for both query and form parameters. 
	
	//@Test //passing multiple values to the params using list
	public void testParameterWithList() {
		
		
		List<String> list = new ArrayList<String>();
		
		list.add("one");
		list.add("two");
		
		given()
			.formParam("A","A value","A value2","A value3")
			.formParam("B",  "B values")
			.formParam("c", list)
		.when()
			.post("http://api.fonts.com/rest/json/Domains/")
		.then()
			.statusCode(400);
	}
	
	@Test // Path parameters passing them in
	public void testPathparameters() {
		
		
		given()
			.pathParam("type","json")
			.pathParam("section","Domains")
		.when()
			.post("http://api.fonts.com/rest/{type}/{section}/")
		.then()
			.statusCode(400);
		
	}
	
	
	//coockeie can be set in request param
	//@Test
	public void testCookieInRequest() {
		
		given()
			.cookie("--utmt","1")
		.when()
			.get("http://wwww.webservicex.com/globalweather.asmx?op=GetCitiesByCountry")
		.then()	
			.statusCode(200);

	//set two cookies
	given().cookie("Key", "Val1","val2"); //This will create two cookeis with key = val1, key=val2
	
	//to set detailed cookie
	Cookie cookie = new Cookie.Builder("cookiename", "cookievalue").setSecured(true).setComment("Test").build();
	given().cookie(cookie).when().get("/cookie").then().assertThat().body(equalTo("X"));
	}
	
	
}

	


	
