package commonBase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class baseclass {
	
	private WebDriver Driver;
	static String driverpath = "C:\\Users\\Localuser\\.m2\\repository\\org\\seleniumhq\\selenium\\selenium-chrome-driver\\3.10.0\\selenium-chrome-driver-3.10.0.jar";
	
	public WebDriver getDriver ()
	{
		setDriver();
			return Driver;

	}
	
	public WebDriver setDriver()
	{

			System.out.println("Browser :Is invalid to lauch, hence default browser is chrome.");
			return Driver = InitChrome();

	}
	
	public WebDriver initfirefox() {
		// TODO Auto-generated method stub
		
		return Driver;
	}

	
	private WebDriver InitChrome() {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", driverpath);
		
		WebDriver Driver = new ChromeDriver();
		Driver.manage().window().maximize();
		goto_url();
		return Driver;
	}

	
	public void goto_url()
	{	
		Driver.get("http://newtours.demoaut.com/");
	}
	

}
