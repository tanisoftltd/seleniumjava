package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageObjects {

	@FindBy(name = "firstName")
    WebElement firstName;
	
    @FindBy(name = "lastName")
    WebElement lastName;

    @FindBy(name = "email")
    WebElement userName;

    @FindBy(name = "password")
    WebElement password;

    @FindBy(name = "confirmPassword")
    WebElement confirmPassword;

    @FindBy(name = "register")
    WebElement submit;
    
    WebDriver driver;
	
	public LoginPageObjects(WebDriver driver) {
		
		this.driver = driver;
		
        PageFactory.initElements(driver, this);
        //RegistrationPageObject RegistrationPageObject = new RegistrationPageObject();
    
    }
	
    public void setFirstName(String fname) {
        
    	firstName.sendKeys("fname");
    	   	
    }

    public void setLastName(String lname) {
    	lastName.sendKeys(lname);
        
    }

    public void setUserName(String uname) {
        this.userName.sendKeys(uname);
        
    }

    public void setPassword(String pword) {
        this.password.sendKeys(pword);
        
    }

    public void setConfirmPassword(String confimpwd) {
        this.confirmPassword.sendKeys(confimpwd);
        
    }

    public void submit() {
        this.submit.click();
    }
   
}
