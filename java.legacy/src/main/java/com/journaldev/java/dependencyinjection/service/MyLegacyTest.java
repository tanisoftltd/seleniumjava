package com.journaldev.java.dependencyinjection.service;

public class MyLegacyTest 
{
    public static void main( String[] args )
    {
    	EmailService emil = new EmailService();
        MyApplication app = new MyApplication(emil);
             		
        app.processMessage("Hi Subbu", "Subbutest@gmail.com");
       
    }
}
