package com.journaldev.java.dependencyinjection.service;

import java.lang.annotation.*;

public class SMSserviceImpl implements MessageService {
	
	@Override
	public void sendMessage(String msg, String rec) {
		
		System.out.println("SMS sent to"+rec+"with Message ="+msg);
	}

}
