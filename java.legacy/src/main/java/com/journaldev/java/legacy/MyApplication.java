package com.journaldev.java.legacy;

public class MyApplication {
	private EmailService email = null;
	
	public MyApplication(EmailService svc) {
		this.email= svc;
	}
	public void processMessage(String msg, String rec) {
		
		this.email.sendEmail(msg, rec);		
		
	}
}
