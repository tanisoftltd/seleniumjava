package com.java.learning.browerstack;
import java.net.*;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.DataProvider;

public class browserstack 
{

	public static WebDriver driver;
	public static String UserName = "rajkumar51";
	public static String Accesskey = "16Apxs7gXakLBaE9Z4xx";
	public static String URL = "htps://"+UserName+Accesskey+"@hub.browserstack.com/wd/hub";
	
	public WebDriver getDriver() throws MalformedURLException {
		
		//this.driver = driver(browerStackURL,capability);
		URL browerStackURL = new URL(URL);
		
		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setPlatform(Platform.WINDOWS);
		capability.setBrowserName("Chrome");
		capability.setVersion("62.0");
		capability.setCapability("browserStack.debug", true);
		
		//this.driver = driver.RemoteWebDriver(browerStackURL,capability);
		WebDriver driver = new RemoteWebDriver(browerStackURL,capability );

		return driver;
	}
	
	@DataProvider (name="stackdata")
	public Object[][] getData()
	{
		Object[][] testData = new Object[][] {
			{Platform.MAC, "chrome","62.0"},
			{Platform.WIN8, "chrome","62.0"},
			{Platform.WINDOWS, "chrome","62.0"}
	
		};
		return testData;
	}
	
	
}
