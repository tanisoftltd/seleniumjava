package com.java.learning.learnOOPs;

//Multiple inheritance issue and ambiguity between methods in interface.
//https://www.youtube.com/watch?v=t4GstSJcD4Y

interface oneone
{
	void abc();
	public default void show()
	{
		System.out.println("In interface oneone show");
	}

}

interface twotwo
{
	void abc();
	public default void show()
	{
		System.out.println("In interface twotwo show");
	}

}

class oneandtwo implements oneone, twotwo
{
	public void abc() {
		System.out.println("ABC interface implements");
	}
	//This is one way to avoid conflict between same method name in multiple interface.
	//public void show() {
	//	System.out.println("interface implemented in class");
	//}
	//This is ANOTHER way to avoid conflict between same method name in multiple interface.
		@Override
		public void show() {
			//oneone.super.show();
			twotwo.super.show();
		}
}

public class multipleinterfaceJava8 {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			oneone obj = new oneandtwo();
			obj.show();
			obj.abc();
		}
	
	}


