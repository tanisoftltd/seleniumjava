package com.java.learning.learnOOPs;


class calc
{
//these are instance scope
	int num1;
	int num2;
	int result;
	
	public calc(int num1, int num2) {
		// these variables are local scope
		
		//these are assigned itself.
		
		
		num1 = num1; // return zero
		num2 = num2;
		
		
		//now changed to This keyword to make these variable to local vairable.
		//"This" represent to local object/or current object.
		this.num1 = num1;
		this.num2 = num2;
		
		//instance variable = local variable.
		//*** This - keyword is differenciate instace variable from local variable.
		
	}

}

public class ThisKeyword {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		calc obj = new calc(4,5);
		
		System.out.println(obj.num1);
		System.out.println(obj.num2);
		
	}

}
