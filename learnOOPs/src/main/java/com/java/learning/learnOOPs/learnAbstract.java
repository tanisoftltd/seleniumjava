package com.java.learning.learnOOPs;


abstract class Human
{
	public abstract void eat();
	
	public void walk()
	{
				
	}
}

class man extends Human //called concrete class
{
	public void eat()
	{
		
	}
}


public class learnAbstract {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Human obj = new man();
		obj.eat();
		obj.walk();
	}
}

//---FINAL keyword

final class aa
{
	public final void test()
	{
		
		
	}
}
//final keyword in class name, means no other class can extend your class.
// if i make my method final, no one else can override my method.
// final with variable it will be constant values.



