package com.java.learning.learnOOPs;

//crate a variable in interface is called "Final" or constraint variable.

interface DemoInterface
{
	// final int num=8 
	//is equal to below int num=8;
	int num=8;
	static String subbu = "Subbu";
	void abc();
	static void show()
	{
		System.out.println("hi from, interface");
	}
}
/* class demoimpl implements demo
{
	public void abc()
	{
		//cant create vairable is it defined in interface hence it is "Final" (constraint variable).
		//you cant use assign value.
		//num=9;
		System.out.println("abc is added.");
	}
} */
public class interfaceStatic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(DemoInterface.subbu);
		DemoInterface.show();
		
	}

}
