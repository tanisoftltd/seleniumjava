package com.java.learning.learnOOPs;

//Abstract class -> define and declare.
//Interface -> declare but not define in java 1.7.
//1.8 can define methods in interface.

interface demo
{
	void abc();
	public default void show()
	{
		System.out.println("In interface show");
	}

}

class demoimp implements demo
{
	public void abc() {
		System.out.println("ABC interface implements");
	}
	public void show() {
		System.out.println("ABC interface implements");
	}
}
public class InterfaceJava8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		demo obj = new demoimp();
		obj.show();
		obj.abc();
	}

}
