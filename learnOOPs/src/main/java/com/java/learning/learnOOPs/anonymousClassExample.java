package com.java.learning.learnOOPs;

//interfaces are 3 types
// 1. Normal - which can have multiple methods in it.
// 2. Single abstract method or functional interface in java8 - lambda expression from scala
// 3. Maker interface - which doesn't have single method declaration.

@FunctionalInterface
interface functioninterfaceA{
	
	void show();
}


class superA
{
	public void show()
	{
		System.out.println("In a Show Method");
	}
}

class ExtendB extends superA
{
	public void show() {
		System.out.println("I am the best part from extend class");
	}
	
}
public class anonymousClassExample {

	public static void main(String[] args)
	{
		//if you want to override you can write different class with same method.
		//Normal override using extend super class.
		
		ExtendB obj = new ExtendB();
		obj.show();
		
		//same as above example can be done through below anonymous class.
		
		superA obj1 = new superA()
						{
							public void show()
							{
								System.out.println("I am the Best");
							}
					
						};
				obj1.show();
				
		//below code to access interface with anonymous class. this can be done for functionalInterface 
			// as it does contain single method hence it mapped without specify what to map exactly.
				
				functioninterfaceA obj3 =()-> System.out.println("I am from functional interface");
				obj3.show();
	}
}
