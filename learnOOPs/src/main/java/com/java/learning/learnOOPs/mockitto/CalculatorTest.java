//https://www.youtube.com/watch?v=HsQ9OwKA79s

package com.java.learning.learnOOPs.mockitto;

import static org.testng.Assert.assertEquals;

import org.mockito.Mock;
import org.mockito.Mockito.*;
import org.testng.annotations.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
public class CalculatorTest {

	
	calculator calc = null;
	//One way it to create an object for interface using service class
	/*
	CalculatorService service = new CalculatorService() {
		public int add(int i, int j) {
			return 0;
			
		}
	};
	*/
	
	CalculatorService service = mock(CalculatorService.class);

	
	
	@Test
	public void testPerform()
	{
		calc = new calculator(service);
		
		calc.perform(2,3);
		when(service.add(5,10)).thenReturn(15);
		assertEquals(75, calc.perform(5, 10));
	
		verify(service).add(5,10);
		
	}
}
