package com.java.learning.learnOOPs.mockitto;

public interface CalculatorService {

	int add(int i, int j);
	
}
