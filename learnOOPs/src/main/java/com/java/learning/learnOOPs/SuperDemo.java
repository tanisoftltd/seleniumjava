package com.java.learning.learnOOPs;


class A{
	
	//crated constructor
	public A() {
		System.out.println("in A");
	}
	
	public A(int i) {
		System.out.println("in INT A");
	}
	
}

class B  extends A
{
	
	//crated constructor
	public B() {
		super(); //by default it will be called super() method to call constructor.
		System.out.println("in B");
	}
	
	public B(int i) {
		
		 // this will be called constructor all the time - By default.
		//super(); 
		
		// this will always called super'calss int method.
		super(5); 
		
		
		System.out.println("in INT B");
	}
	
}
public class SuperDemo {
	
	public static void main(String[] args) {
		//run A object it runs just A
		A obj = new A();
		
		//run B object Extends A then runs just A and B
		// it called subclass and superclass constructors
		B obj1 = new B();
		
		// This will called, B int mehtod and class A constructor.
		B obj2 = new B(5);

		
	}

}
