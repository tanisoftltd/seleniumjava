package com.java.learning.learnOOPs;

//Very important video, well explain about interface, abstract class and 
//class can be extended, interface must be implements.
//https://www.youtube.com/watch?v=sZr8G7sYGzU

/*
class Writer
{
	public void write()
	{
		
	}
}
abstract class Writer
{
	public abstract void write();
}*/

interface Writer
{
	void write();
} 




//above any one option can be used to support, either abstarct class or interface
//==================
class Pen implements Writer
{
	public void write() {
		System.out.println("I am a Pen");
	}
}

class Pencil implements Writer
{
	public void write() {
		System.out.println("I am a Pencil");
	}

}

class Kit
{
	public void doSomething(Writer p) 
	{
		p.write();
		
	}
	
}

public class interfaceexplain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Kit kit = new Kit();
		Pen pen = new Pen();
		Pencil pc = new Pencil();
		
		kit.doSomething(pen);
		kit.doSomething(pc);
		
		
	}

}
