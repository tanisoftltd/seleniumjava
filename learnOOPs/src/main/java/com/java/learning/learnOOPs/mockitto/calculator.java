package com.java.learning.learnOOPs.mockitto;


public class calculator {
	
	
	CalculatorService service;
	
	public calculator(CalculatorService service)
	{
		this.service = service;
	}

	public int perform(int i, int j)
	{
		//return (i+j)*i;
		return service.add(i, j)*i;
		
	}

}
